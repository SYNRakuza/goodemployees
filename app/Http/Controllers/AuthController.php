<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login() {
    	return view('auth.login');
    }

    public function postlogin(Request $request) {
    	if(Auth::attempt($request->only('nip','password'))){
    		return redirect('/');
    	}

    	return redirect('/login')->with(['error' => 'NIP atau Password salah!!']);
    }

    public function logout(){
    	Auth::logout();
    	return redirect('/login');
    }
}
