<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PegawaiController extends Controller
{
    public function index(){
        $data_pegawai =\App\Pegawai::where('user_id', auth()->user()->id)->get();
    	return view('pegawai.index',[
            'data_pegawai'      => $data_pegawai,
    		'title'				=> 'Profile',	
    		'nav'				=> 'Profile']);
    }
//**BEGIN MY PROFILE**//
    public function editprofil(Request $request, $id_pegawai){
        $data_pegawai = \App\Pegawai::find($id_pegawai);
        $datapegawai = \App\Pegawai::where('user_id', $data_pegawai->user_id);
        $datapegawai->update(array(
            'nama_pegawai'      => $request->nama_pegawai,
            'alamat_pegawai'    => $request->alamat_pegawai,
            'email_pegawai'     => $request->email_pegawai,
            'no_hp_pegawai'     => $request->no_hp_pegawai,
            ));

        $user = \App\User::where('id', $data_pegawai->user_id);
        if($request->password != '') {
            if($request->file('foto') != ''){
                $user->update([
                'name'              => $request->nama_pegawai,
                'password'          => bcrypt($request->password),
                'email'             => $request->email_pegawai,
                'foto'              => $request->file('foto')->getClientOriginalName(),
                ]);
            }
            else{
                $user->update([
                'name'              => $request->nama_pegawai,
                'password'          => bcrypt($request->password),
                'email'             => $request->email_pegawai,
                ]);
            }
        }
        else{
            if($request->file('foto') != ''){
                $user->update([
                'name'              => $request->nama_pegawai,
                'password'          => $request->password_lama,
                'email'             => $request->email_pegawai,
                'foto'              => $request->file('foto')->getClientOriginalName(),
                ]);
            }
            else{
                $user->update([
                'name'              => $request->nama_pegawai,
                'password'          => $request->password_lama,
                'email'             => $request->email_pegawai,
                ]);   
            }
        }

        if($request->hasFile('foto')){
            $request->file('foto')->move('assets/images/avatar/', $request->file('foto')->getClientOriginalName());
            $user->foto = $request->file('foto')->getClientOriginalName();
            $user->save();
        }

        return redirect('/');
    }
//**END MY PROFILE**//
//**BEGIN DATABASE PEGAwAI**//
    public function dbpegawai(){
    	$data_pegawai = \App\Pegawai::all();
    	return view('pegawai.database',[
    		'data_pegawai'	 	=> $data_pegawai,
    		'title'				=> 'Database Pegawai',	
    		'nav'				=> 'Database / Semua Pegawai',
    		'judul'				=> 'Data Semua Pegawai',
    		'controller'		=> 'dbpegawai']);
    }

    public function dbmanager(){
    	$data_pegawai = \App\Pegawai::where('jabatan_pegawai', 'Manager')->get();;
    	return view('pegawai.database',[
    		'data_pegawai'	 	=> $data_pegawai,
    		'title'				=> 'Database Pegawai',	
    		'nav'				=> 'Database / Manager',
    		'judul'				=> 'Data Manager',
    		'controller'		=> 'dbmanager']);
    }

    public function dbkepala(){
    	$data_pegawai = \App\Pegawai::where('jabatan_pegawai', 'Kepala Divisi')->get();;
    	return view('pegawai.database',[
    		'data_pegawai'	 	=> $data_pegawai,
    		'title'				=> 'Database Pegawai',	
    		'nav'				=> 'Database / Kepala Divisi',
    		'judul'				=> 'Data Kepala Divisi',
    		'controller'		=> 'dbkepala']);
    }

    public function dbkeuangan(){
    	$data_pegawai = \App\Pegawai::where('bagian_pegawai', 'Keuangan')->get();;
    	return view('pegawai.database',[
    		'data_pegawai'	 	=> $data_pegawai,
    		'title'				=> 'Database Pegawai',	
    		'nav'				=> 'Database / Divisi Keuangan',
    		'judul'				=> 'Data Divisi Keuangan',
    		'controller'		=> 'dbkeuangan']);
    }

    public function dbpenjualan(){
    	$data_pegawai = \App\Pegawai::where('bagian_pegawai', 'Penjualan')->get();;
    	return view('pegawai.database',[
    		'data_pegawai'	 	=> $data_pegawai,
    		'title'				=> 'Database Pegawai',	
    		'nav'				=> 'Database / Divisi Penjualan',
    		'judul'				=> 'Data Divisi Penjualan',
    		'controller'		=> 'dbpenjualan']);
    }

    public function dbpemasaran(){
    	$data_pegawai = \App\Pegawai::where('bagian_pegawai', 'Pemasaran')->get();;
    	return view('pegawai.database',[
    		'data_pegawai'	 	=> $data_pegawai,
    		'title'				=> 'Database Pegawai',	
    		'nav'				=> 'Database / Divisi Pemasaran',
    		'judul'				=> 'Data Divisi Pemasaran',
    		'controller'		=> 'dbpemasaran']);
    }

    public function dbinformasi(){
    	$data_pegawai = \App\Pegawai::where('bagian_pegawai', 'Informasi dan Teknologi')->get();;
    	return view('pegawai.database',[
    		'data_pegawai'	 	=> $data_pegawai,
    		'title'				=> 'Database Pegawai',	
    		'nav'				=> 'Database / Divisi Informasi dan Teknologi',
    		'judul'				=> 'Data Divisi Informasi dan Teknologi',
    		'controller'		=> 'dbinformasi']);
    }

    public function create(Request $request){
    	//Insert table User
    	$user = new \App\User;
    	$user->role = $request->jabatan_pegawai;
    	$user->name = $request->nama_pegawai;
    	$user->email = $request->email_pegawai;
    	$user->nip = $request->nip_pegawai;
    	$user->password = bcrypt('good');
    	$user->remember_token = Str::random(60);
    	$user->save();


    	//Insert table pegawai
    	$request->request->add(['user_id' => $user->id]);
    	$pegawai = \App\Pegawai::create($request->all());

    	//Insert table Jobdesk
    	// $jobdesk = new \App\Jobdesk;
    	// $jobdesk->pegawai_id = $pegawai->id_pegawai;
    	// $jobdesk->nama_pegawai = $pegawai->nama_pegawai;
    	// $jobdesk->save();

    	$con = $request->kategori_pegawai;
    	return redirect('/'.$con)->with('sukses', 'Data Berhasil di Tambahkan');
    }

    public function edit(Request $request, $id_pegawai){
    	$pegawai = \App\Pegawai::find($id_pegawai);
    	$pegawai->update($request->all());
    	$con = $request->kategori_pegawai;
    	return redirect('/'.$con)->with('sukses', 'Data Berhasil di Update');
    }

    public function ban(Request $request, $id_pegawai){
    	$pegawai = \App\Pegawai::find($id_pegawai);
    	$con = $request->kategori_pegawai;
    	return redirect('/'.$con)->with('sukses', 'Anda Tidak Mempunyai Hak');
    }

    public function delete(Request $request, $id_pegawai){
    	$pegawai = \App\Pegawai::find($id_pegawai);
    	$pegawai->delete();
    	$con = $request->kategori_pegawai;
    	return redirect('/'.$con)->with('sukses', 'Data Berhasil di Hapus');
    }
//**END DATABASE PEGAwAI**//

//**BEGIN JOBDESK PEGAWAI**//
    public function jdmanager(){
    	$data_pegawai = \App\Pegawai::where('jabatan_pegawai', 'Manager')->get();;
    	return view('pegawai.jobdesk',[
    		'data_pegawai'	 	=> $data_pegawai,
    		'title'				=> 'Jobdesk Pegawai',	
    		'nav'				=> 'Jobdesk / Manager',
    		'judul'				=> 'Daftar Manager',
    		'controller'		=> 'jdmanager']);
    }

    public function jdkepala(){
    	$data_pegawai = \App\Pegawai::where('jabatan_pegawai', 'Kepala Divisi')->get();;
    	return view('pegawai.jobdesk',[
    		'data_pegawai'	 	=> $data_pegawai,
    		'title'				=> 'Jobdesk Pegawai',	
    		'nav'				=> 'Jobdesk / Kepala Divisi',
    		'judul'				=> 'Daftar Kepala Divisi',
    		'controller'		=> 'jdkepala']);
    }

    public function jdkeuangan(){
    	$data_pegawai = \App\Pegawai::where('bagian_pegawai', 'Keuangan')->where('jabatan_pegawai', 'Divisi')->get();
    	return view('pegawai.jobdesk',[
    		'data_pegawai'	 	=> $data_pegawai,
    		'title'				=> 'Jobdesk Pegawai',	
    		'nav'				=> 'Jobdesk / Divisi Keuangan',
    		'judul'				=> 'Daftar Divisi Keuangan',
    		'controller'		=> 'jdkeuangan']);
    }

    public function jdpenjualan(){
    	$data_pegawai = \App\Pegawai::where('bagian_pegawai', 'Penjualan')->where('jabatan_pegawai', 'Divisi')->get();
    	return view('pegawai.jobdesk',[
    		'data_pegawai'	 	=> $data_pegawai,
    		'title'				=> 'Jobdesk Pegawai',	
    		'nav'				=> 'Jobdesk / Divisi Penjualan',
    		'judul'				=> 'Daftar Divisi Penjualan',
    		'controller'		=> 'jdpenjualan']);
    }

    public function jdpemasaran(){
    	$data_pegawai = \App\Pegawai::where('bagian_pegawai', 'Pemasaran')->where('jabatan_pegawai', 'Divisi')->get();
    	return view('pegawai.jobdesk',[
    		'data_pegawai'	 	=> $data_pegawai,
    		'title'				=> 'Jobdesk Pegawai',	
    		'nav'				=> 'Jobdesk / Divisi Pemasaran',
    		'judul'				=> 'Daftar Divisi Pemasaran',
    		'controller'		=> 'jdpemasaran']);
    }

    public function jdinformasi(){
    	$data_pegawai = \App\Pegawai::where('bagian_pegawai', 'Informasi dan Teknologi')->where('jabatan_pegawai', 'Divisi')->get();
    	return view('pegawai.jobdesk',[
    		'data_pegawai'	 	=> $data_pegawai,
    		'title'				=> 'Jobdesk Pegawai',	
    		'nav'				=> 'Jobdesk / Divisi Informasi dan Teknologi',
    		'judul'				=> 'Daftar Divisi Informasi dan Teknologi',
    		'controller'		=> 'jdinformasi']);
    }

    public function jobdesk($id_pegawai){
    	$pegawai = \App\Jobdesk::where('pegawai_id', $id_pegawai)->get();;
    	$namapegawai = \App\Pegawai::where('user_id', $id_pegawai)->get();;
    	return view('pegawai.jobdeskpegawai',[
    		'data_pegawai'	 	=> $pegawai,
    		'title'				=> 'Jobdesk Pegawai',
    		'namapegawai'		=> $namapegawai,	
    		'nav'				=> 'Jobdesk',]);

    }

    public function createjobdesk(Request $request, $id_pegawai){
    	$pegawai = \App\Jobdesk::create($request->all());
    	$con = $request->pegawai_id;

    	return redirect('pegawai/'.$con.'/jobdesk')->with('sukses', 'Jobdesk Berhasil di Tambahkan');

    }

    public function editjobdesk(Request $request, $id_jobdesk){
    	$pegawai = \App\Jobdesk::find($id_jobdesk);
    	$pegawai->update($request->all());
    	$con = $request->pegawai_id;

    	return redirect('pegawai/'.$con.'/jobdesk')->with('sukses', 'Jobdesk Berhasil di Perbaharui');
    }

    public function statusjobdesk(Request $request, $id_jobdesk){
    	$pegawai = \App\Jobdesk::find($id_jobdesk);
    	$namapegawai = \App\Pegawai::where('user_id', $pegawai->pegawai_id);
    	$namapegawai->update(array('kinerja_pegawai' => $request->kinerja_pegawai));
    	$pegawai->update(array('status_jobdesk' => $request->status_jobdesk));
    	$con = $pegawai->pegawai_id;

    	return redirect('pegawai/'.$con.'/jobdesk')->with('sukses', 'Jobdesk Berhasil di Perbaharui');
    }

//**END JOBDESK PEGAWAI**//

//**BEGIN MY JOBDESK**//
    
    public function myjobdesk($id){
    	$pegawai = \App\Jobdesk::where('pegawai_id', $id)->get();;
    	$namapegawai = \App\Pegawai::where('user_id', $id)->get();;
    	return view('pegawai.myjobdesk',[
    		'data_pegawai'	 	=> $pegawai,
    		'title'				=> 'Jobdesk Pegawai',
    		'namapegawai'		=> $namapegawai,	
    		'nav'				=> 'Jobdesk',]);

    }
    

//**END MY JOBDESK**//
//**BEGIN AKUN**//
    public function akun(){
        $data_admin = \App\User::where('role', 'Admin')->get();
        return view('pegawai.admin',[
            'data_admin'      => $data_admin,
            'title'             => 'Daftar Admin',  
            'nav'               => 'Admin / Semua Admin',
            'judul'             => 'Daftar Semua Semua', ]);
    }

    public function createadmin(Request $request){
        $user = new \App\User;
        $user->role = 'Admin';
        $user->name = $request->name;
        $user->email = $request->email;
        $user->nip = $request->nip;
        $user->password = bcrypt('good');
        $user->remember_token = Str::random(60);
        $user->save();

        // Insert table Pegawai
        $pegawai = new \App\Pegawai;
        $pegawai->user_id = $user->id;
        $pegawai->nip_pegawai = $request->nip;
        $pegawai->email_pegawai = $request->email;
        $pegawai->nama_pegawai = $request->name;
        $pegawai->jabatan_pegawai = 'Admin';
        $pegawai->save();

        return redirect('/akun')->with('sukses', 'Admin Berhasil di Tambahkan');
    }

    public function editadmin(Request $request, $id){
        $admin = \App\User::find($id);
        $admin->update($request->all());

        $pegawai = \App\Pegawai::where('user_id', $request->id);
        $pegawai->nama_pegawai = $request->name;
        $pegawai->nip_pegawai = $request->nip;
        $pegawai->email_pegawai = $request->email;
        $pegawai->save();

        return redirect('/akun')->with('sukses', 'Admin Berhasil di Perbaharui');
    }
//**END AKUN**//
}
