<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobdesk extends Model
{
    protected $table = 'jobdesk';
    protected $primaryKey = 'id_jobdesk';
    protected $fillable = ['pegawai_id', 'nama_pegawai', 'nama_jobdesk', 'tipe_jobdesk', 'deskripsi_jobdesk', 'status_jobdesk', 'waktu_jobdesk', 'bobot_jobdesk', 'kategori_pegawai'];

    public $timestamps = false;
}
