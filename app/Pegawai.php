<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Pegawai extends Model
{
    protected $table = 'pegawai';
    protected $primaryKey = 'id_pegawai';
    protected $fillable = ['nip_pegawai', 'nama_pegawai', 'jabatan_pegawai', 'alamat_pegawai', 'email_pegawai', 'no_hp_pegawai', 'gaji_pegawai', 'bagian_pegawai', 'kategori_pegawai', 'user_id', 'kinerja_pegawai'];

    public $timestamps = false;
}
