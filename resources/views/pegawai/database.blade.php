@extends('layouts.master')  
        @section('content')
<div class="content mt-3">
            <div class="animated fadeIn">
            @if($message = Session::get('sukses'))
            <div class="alert alert-success" role="alert">
                {{$message}}
            </div>
            @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">{{$judul}}</strong>
                                <?php if($controller == 'dbpegawai') { ?> 
                                <button type="button" class="btn btn-success btn-sm float-right" data-toggle="modal" data-target="#tambahModal">
                                <span class="btn-icon-right"><i class="fa fa-plus-square"></i></span>
                                </button>
                                <?php } ?> 
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;vertical-align: middle;">No.</th>
                                            <th style="text-align: center;vertical-align: middle;">NIP</th>
                                            <th style="text-align: center;vertical-align: middle;">Nama</th>
                                            <th style="text-align: center;vertical-align: middle;">Jabatan</th>
                                            <th style="text-align: center;vertical-align: middle;">Email</th>
                                            <th style="text-align: center;vertical-align: middle;">action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1 ?>
                                        @foreach($data_pegawai as $pegawai)
                                        <tr>
                                            <td style="vertical-align: middle;text-align: center;">{{$no++}}</td>
                                            <td style="vertical-align: middle;text-align: left;">{{$pegawai->nip_pegawai}}</td>
                                            <td style="vertical-align: middle;text-align: left;">{{$pegawai->nama_pegawai}}</td>
                                            <td style="vertical-align: middle;text-align: left;">{{$pegawai->jabatan_pegawai}} {{$pegawai->bagian_pegawai}}</td>
                                            <td style="vertical-align: middle;text-align: center;">{{$pegawai->email_pegawai}}</td>
                                            <td>
                                                <button type="button" class="btn mb-1 btn-info btn-sm" data-toggle="modal" data-target="#detailModal{{$pegawai->id_pegawai}}"><i class="fa fa-info-circle"></i>
                                                </button>
                                                <button type="button" class="btn mb-1 btn-primary btn-sm" data-toggle="modal" data-target="#editModal{{$pegawai->id_pegawai}}"><i class="fa fa-pencil"></i>
                                                </button>
                                                <button type="button" class="btn mb-1 btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal{{$pegawai->id_pegawai}}" ><i class="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
    </div>

<!-- Modal Tambah -->
<div class="modal fade" id="tambahModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="largeModalLabel">Tambah Data Pegawai</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="basic-form">
                            <form action="/pegawai/create" method="POST">
                            {{csrf_field()}}
                            <div class="form-row">
                               <div class="col-7">
                                    <label>Nama Pegawai :</label>
                                    <input type="text" class="form-control" name="nama_pegawai" required maxlength="50">
                               </div>
                               <div class="col-5">
                                    <label>NIP Pegawai :</label>
                                    <input type="text" class="form-control" name="nip_pegawai" required maxlength="50">
                               </div>
                            </div>
                    </div>
                    </br>
                    <div class="basic-form">
                        <div class="form-row">
                            <div class="col-3">
                                <label>Jabatan :</label>
                                <select class="form-control" name="jabatan_pegawai">
                                    <option value="Manager">Manager</option>
                                    <option value="Kepala Divisi">Kepala Divisi</option>
                                    <option value="Divisi">Anggota Divisi</option>
                                </select>
                            </div>
                            <div class="col-6">
                                <label>Bagian :</label>
                                <select class="form-control" name="bagian_pegawai">
                                    <option value="Umum">Umum</option>
                                    <option value="Keuangan">Keuangan</option>
                                    <option value="Penjualan">Penjualan</option>
                                    <option value="Pemasaran">Pemasaran</option>
                                    <option value="Informasi dan Teknologi">Informasi dan Teknologi</option>
                                </select>
                            </div>
                            <div class="col-3">
                                <label>Gaji :</label>
                                <input type="number" class="form-control" name="gaji_pegawai" maxlength="50">
                            </div>
                        </div>
                    </div>
                    </br>
                    <div class="basic-form">
                        <div class="form-row">
                            <div class="col-5">
                                <label>Alamat :</label>
                                <input type="text" class="form-control" name="alamat_pegawai" maxlength="50">
                            </div>
                            <div class="col-4">
                                <label>Email :</label>
                                <input type="email" class="form-control" name="email_pegawai" maxlength="50">
                            </div>
                            <div class="col-3">
                                <label>No. HP :</label>
                                <input type="number" class="form-control" name="no_hp_pegawai" maxlength="50">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="kinerja_pegawai" value="0">
                    <input type="hidden" name="kategori_pegawai" value="<?= $controller?>">
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success">Tambah</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
<!-- End Modal tambah -->

<!-- Modal Detail -->
@foreach($data_pegawai as $pegawai)
<?php $select1 = $pegawai->jabatan_pegawai;
$select2 = $pegawai->bagian_pegawai; ?>
<div class="modal fade" id="detailModal{{$pegawai->id_pegawai}}" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="largeModalLabel">Detail Data Pegawai</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="basic-form">
                            <form action="/pegawai/create" method="POST">
                            {{csrf_field()}}
                            <div class="form-row">
                               <div class="col-7">
                                    <label>Nama Pegawai :</label>
                                    <input type="text" class="form-control" name="nama_pegawai" readonly value="{{$pegawai->nama_pegawai}}" maxlength="50">
                               </div>
                               <div class="col-5">
                                    <label>NIP Pegawai :</label>
                                    <input type="text" class="form-control" name="nip_pegawai" readonly value="{{$pegawai->nip_pegawai}}" maxlength="50">
                               </div>
                            </div>
                    </div>
                    </br>
                    <div class="basic-form">
                        <div class="form-row">
                            <div class="col-3">
                                <label>Jabatan :</label>
                                <input type="text" class="form-control" name="jabatan_pegawai" readonly maxlength="50" value="{{$select1}}">
                            </div>
                            <div class="col-6">
                                <label>Bagian :</label>
                                <input type="text" class="form-control" name="bagian_pegawai" readonly maxlength="50" value="{{$select2}}">
                            </div>
                            <div class="col-3">
                                <label>Gaji :</label>
                                <input type="number" class="form-control" name="gaji_pegawai" readonly value="{{$pegawai->gaji_pegawai}}" maxlength="50">
                            </div>
                        </div>
                    </div>
                    </br>
                    <div class="basic-form">
                        <div class="form-row">
                            <div class="col-5">
                                <label>Alamat :</label>
                                <input type="text" class="form-control" name="alamat_pegawai" readonly value="{{$pegawai->alamat_pegawai}}" maxlength="50">
                            </div>
                            <div class="col-4">
                                <label>Email :</label>
                                <input type="email" class="form-control" name="email_pegawai" readonly value="{{$pegawai->email_pegawai}}" maxlength="50">
                            </div>
                            <div class="col-3">
                                <label>No. HP :</label>
                                <input type="number" class="form-control" name="no_hp_pegawai" readonly value="{{$pegawai->no_hp_pegawai}}" maxlength="50">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="kategori_pegawai" value="<?= $controller?>">
                            </div>
                        </div>
                    </div>
                </div>
@endforeach
<!-- End Modal Detail -->

<!-- Modal Edit -->
@foreach($data_pegawai as $pegawai)
<?php $select1 = $pegawai->jabatan_pegawai;
$select2 = $pegawai->bagian_pegawai; ?>
<div class="modal fade" id="editModal{{$pegawai->id_pegawai}}" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="largeModalLabel">Edit Data Pegawai</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="basic-form">
                            @if(auth()->user()->role == 'Admin')
                            <form action="/pegawai/{{$pegawai->id_pegawai}}/edit" method="POST">
                            @endif
                            <form method="POST" action="/pegawai/{{$pegawai->id_pegawai}}/ban">
                            {{csrf_field()}}
                            <div class="form-row">
                               <div class="col-7">
                                    <label>Nama Pegawai :</label>
                                    <input type="text" class="form-control" name="nama_pegawai" required value="{{$pegawai->nama_pegawai}}" maxlength="50">
                               </div>
                               <div class="col-5">
                                    <label>NIP Pegawai :</label>
                                    <input type="text" class="form-control" name="nip_pegawai" required value="{{$pegawai->nip_pegawai}}" maxlength="50">
                               </div>
                            </div>
                    </div>
                    </br>
                    <div class="basic-form">
                        <div class="form-row">
                            <div class="col-3">
                                <label>Jabatan :</label>
                                <select class="form-control" name="jabatan_pegawai">
                                    <option value="Manager" <?php if($select1 == 'Manager'){echo "selected";} ?>>Manager</option>
                                    <option value="Kepala Divisi" <?php if($select1 == 'Kepala Divisi'){echo "selected";} ?>>Kepala Divisi</option>
                                    <option value="Divisi" <?php if($select1 == 'Divisi'){echo "selected";} ?>>Anggota Divisi</option>
                                </select>
                            </div>
                            <div class="col-6">
                                <label>Bagian :</label>
                                <select class="form-control" name="bagian_pegawai">
                                    <option value="Umum" <?php if($select2 == 'Umum'){echo "selected";} ?>>Umum</option>
                                    <option value="Keuangan" <?php if($select2 == 'Keuangan'){echo "selected";} ?>>Keuangan</option>
                                    <option value="Penjualan" <?php if($select2 == 'Penjualan'){echo "selected";} ?>>Penjualan</option>
                                    <option value="Pemasaran" <?php if($select2 == 'Pemasaran'){echo "selected";} ?>>Pemasaran</option>
                                    <option value="Informasi dan Teknologi" <?php if($select2 == 'Informasi dan Teknologi'){echo "selected";} ?>>Informasi dan Teknologi</option>
                                </select>
                            </div>
                            <div class="col-3">
                                <label>Gaji :</label>
                                <input type="number" class="form-control" name="gaji_pegawai" required value="{{$pegawai->gaji_pegawai}}" maxlength="50">
                            </div>
                        </div>
                    </div>
                    </br>
                    <div class="basic-form">
                        <div class="form-row">
                            <div class="col-5">
                                <label>Alamat :</label>
                                <input type="text" class="form-control" name="alamat_pegawai" value="{{$pegawai->alamat_pegawai}}" maxlength="50">
                            </div>
                            <div class="col-4">
                                <label>Email :</label>
                                <input type="email" class="form-control" name="email_pegawai" required value="{{$pegawai->email_pegawai}}" maxlength="50">
                            </div>
                            <div class="col-3">
                                <label>No. HP :</label>
                                <input type="number" class="form-control" name="no_hp_pegawai" required value="{{$pegawai->no_hp_pegawai}}" maxlength="50">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="kategori_pegawai" value="<?= $controller?>">
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
@endforeach
<!-- End Modal Edit -->

<!-- Modal Hapus -->
@foreach($data_pegawai as $pegawai)
<div class="modal fade" id="deleteModal{{$pegawai->id_pegawai}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="exampleModalLabel">Hapus Data</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if(auth()->user()->role == 'Admin')
                <form method="POST" action="/pegawai/{{$pegawai->id_pegawai}}/delete">
                @endif
                <form method="POST" action="/pegawai/{{$pegawai->id_pegawai}}/ban">
                {{csrf_field()}}
                <h7>
                    Apakah Anda Yakin Menghapus Data Ini ?
                </h7>
            </div>
            <input type="hidden" name="kategori_pegawai" value="<?= $controller?>">
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Hapus</button>
            </div>

        </form>

        </div>
    </div>                                     
</div>
@endforeach
<!-- End Modal Hapus -->
    @endsection