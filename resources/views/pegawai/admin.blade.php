@extends('layouts.master')  
        @section('content')
<div class="content mt-3">
            <div class="animated fadeIn">
            @if($message = Session::get('sukses'))
            <div class="alert alert-success" role="alert">
                {{$message}}
            </div>
            @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">{{$judul}}</strong>
                                <button type="button" class="btn btn-success btn-sm float-right" data-toggle="modal" data-target="#tambahModal">
                                <span class="btn-icon-right"><i class="fa fa-plus-square"></i></span>
                                </button>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;vertical-align: middle;">No.</th>
                                            <th style="text-align: center;vertical-align: middle;">NIP</th>
                                            <th style="text-align: center;vertical-align: middle;">Nama</th>
                                            <th style="text-align: center;vertical-align: middle;">Email</th>
                                            <th style="text-align: center;vertical-align: middle;">action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1 ?>
                                        @foreach($data_admin as $admin)
                                        <tr>
                                            <td style="vertical-align: middle;text-align: center;">{{$no++}}</td>
                                            <td style="vertical-align: middle;text-align: left;">{{$admin->nip}}</td>
                                            <td style="vertical-align: middle;text-align: left;">{{$admin->name}}</td>
                                            <td style="vertical-align: middle;text-align: center;">{{$admin->email}}</td>
                                            <td><center>
                                                <button type="button" class="btn mb-1 btn-primary btn-md" data-toggle="modal" data-target="#editModal{{$admin->id}}"><i class="fa fa-pencil"></i>
                                                </button>
                                                <button type="button" class="btn mb-1 btn-danger btn-md" data-toggle="modal" data-target="#deleteModal{{$admin->id}}" ><i class="fa fa-trash"></i>
                                                </button></center>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
    </div>

<!-- Modal Tambah -->
<div class="modal fade" id="tambahModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="largeModalLabel">Tambah Data Pegawai</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="basic-form">
                            <form action="/admin/createadmin" method="POST">
                            {{csrf_field()}}
                            <div class="form-row">
                                <div class="col-5">
                                    <label>Nama :</label>
                                    <input type="text" class="form-control" name="name" required maxlength="50">
                                </div>
                                <div class="col-4">
                                    <label>NIP :</label>
                                    <input type="text" class="form-control" name="nip" required maxlength="50">
                                </div>
                                <div class="col-3">
                                    <label>Email :</label>
                                    <input type="email" class="form-control" name="email" required maxlength="50">
                                </div>
                            </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success">Tambah</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
<!-- End Modal tambah -->

<!-- Modal Edit -->
@foreach($data_admin as $admin)
<div class="modal fade" id="editModal{{$admin->id}}" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="largeModalLabel">Edit Data Pegawai</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="basic-form">
                            <form action="/admin/{{$admin->id}}/editadmin" method="POST">
                            {{csrf_field()}}
                            <div class="form-row">
                                <div class="col-5">
                                    <label>Nama :</label>
                                    <input type="text" class="form-control" name="name" required value="{{$admin->name}}" maxlength="50">
                                </div>
                                <div class="col-4">
                                    <label>NIP :</label>
                                    <input type="text" class="form-control" name="nip" required value="{{$admin->nip}}" maxlength="50">
                                </div>
                                <div class="col-3">
                                    <label>Email :</label>
                                    <input type="email" class="form-control" name="email" required value="{{$admin->email}}" maxlength="50">
                                </div>
                            </div>
                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
@endforeach
<!-- End Modal Edit -->

<!-- Modal Hapus -->
@foreach($data_admin as $admin)
<div class="modal fade" id="deleteModal{{$admin->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="exampleModalLabel">Hapus Data</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="/admin/{{$admin->id}}/deleteakun">
                <!-- <form method="POST" action="/admin/{{$admin->id}}/ban"> -->
                {{csrf_field()}}
                <h7>
                    Apakah Anda Yakin Menghapus Admin Ini ?
                </h7>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Hapus</button>
            </div>

        </form>

        </div>
    </div>                                     
</div>
@endforeach
<!-- End Modal Hapus -->
    @endsection