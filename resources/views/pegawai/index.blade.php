        
@extends('layouts.master')  
        @section('content') 
                <div class="content mt-3">
            <div class="animated fadeIn">

    <div class="col-lg-5">
        <div class="card">
            <div class="card-header">
                <strong class="card-title mb-3">Profile Card</strong>
            </div>
            <div class="card-body">
                <div class="mx-auto d-block">
                    <img class="rounded-circle mx-auto d-block" src="{{('assets/images/avatar/'.auth()->user()->foto)}}" alt="Foto Profil">
                    <h5 class="text-sm-center mt-2 mb-1">{{auth()->user()->name}}</h5>
                    @foreach($data_pegawai as $pegawai)
                    <div class="location text-sm-center"><i class="fa fa-map-marker"> {{$pegawai->alamat_pegawai}}</i></div>
                    
                </div>
                <hr>
                <div class="card-text text-sm-center">
                    <button type="button" class="btn mb-1 btn-primary btn-md" data-toggle="modal" data-target="#editModal{{$pegawai->user_id}}">Edit Profil</i>
                    </button>
                </div>
                    @endforeach
            </div>
        </div>
    </div>

    <div class="col-lg-7">
        <div class="card">
            <div class="card-body card-block">
                @foreach($data_pegawai as $pegawai)
                <form action="" method="post" enctype="" class="form-horizontal">
                    <div class="row form-group">
                        <div class="col col-md-3"><label class=" form-control-label">Nama</label></div>
                        <div class="col-12 col-md-9"><input type="text" disabled value="{{$pegawai->nama_pegawai}}" class="form-control"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">NIP</label></div>
                        <div class="col-12 col-md-9"><input type="text" disabled value="{{$pegawai->nip_pegawai}}" class="form-control"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Jabatan</label></div>
                        <div class="col-12 col-md-9"><input type="text" disabled value="{{$pegawai->jabatan_pegawai}} {{$pegawai->bagian_pegawai}}" class="form-control"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="email-input" class=" form-control-label">email</label></div>
                        <div class="col-12 col-md-9"><input type="email" disabled value="{{$pegawai->email_pegawai}}" class="form-control"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Alamat</label></div>
                        <div class="col-12 col-md-9"><input type="text" disabled value="{{$pegawai->alamat_pegawai}}"  class="form-control"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">No.HP</label></div>
                        <div class="col-12 col-md-9"><input type="text" disabled value="{{$pegawai->no_hp_pegawai}}" class="form-control"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Gaji</label></div>
                        <div class="col-12 col-md-9"><input type="text" disabled value="{{$pegawai->gaji_pegawai}}" class="form-control"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">Poin Kinerja</label></div>
                        <div class="col-12 col-md-9"><input type="text" disabled value="{{$pegawai->kinerja_pegawai}}" class="form-control"></div>
                    </div>
                </form>
                @endforeach
             </div>
        </div>
    </div>





    </div><!-- .row -->
    </div><!-- .animated -->
    </div><!-- .content -->




    </div>
<!-- Modal Edit -->
@foreach($data_pegawai as $pegawai)
<div class="modal fade" id="editModal{{$pegawai->user_id}}" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="largeModalLabel">Edit Profil</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="basic-form">
                            <form method="POST" action="/profil/{{$pegawai->id_pegawai}}/editprofil" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-row">
                               <div class="col-7">
                                    <label>Nama :</label>
                                    <input type="text" class="form-control" name="nama_pegawai" required value="{{$pegawai->nama_pegawai}}" maxlength="50">
                               </div>
                               <div class="col-5">
                                    <label>Password :</label>
                                    <input type="password" class="form-control" name="password" maxlength="50">
                                    <input type="hidden" class="form-control" name="password_lama" value="{{auth()->user()->password}}" maxlength="50">
                               </div>
                            </div>
                    </div>
                    </br>
                    <div class="basic-form">
                        <div class="form-row">
                            <div class="col-5">
                                <label>Alamat :</label>
                                <input type="text" class="form-control" name="alamat_pegawai" value="{{$pegawai->alamat_pegawai}}" maxlength="50">
                            </div>
                            <div class="col-4">
                                <label>Email :</label>
                                <input type="email" class="form-control" name="email_pegawai" required value="{{$pegawai->email_pegawai}}" maxlength="50">
                            </div>
                            <div class="col-3">
                                <label>No. HP :</label>
                                <input type="number" class="form-control" name="no_hp_pegawai" value="{{$pegawai->no_hp_pegawai}}" maxlength="50">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="basic-form" style="align:center;">
                        <div class="form-row">
                            <div class="col-4">
                                <label>Foto :</label>
                                <input type="file" class="form-control" name="foto" value="" maxlength="50">
                            </div>
                        </div>
                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
@endforeach
<!-- End Modal Edit -->
    @endsection