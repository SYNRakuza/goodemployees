@extends('layouts.master')  
        @section('content')
<div class="content mt-3">
            <div class="animated fadeIn">
             @if($message = Session::get('sukses'))
            <div class="alert alert-success" role="alert">
                {{$message}}
            </div>
            @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                            @foreach($namapegawai as $nama)
                                <strong class="card-title">{{$nama->nama_pegawai}} / {{$nama->jabatan_pegawai}} {{$nama->bagian_pegawai}}</strong>
                                <br>
                                <strong class="card-title">Daftar Jobdesk</strong>
                            @endforeach
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;vertical-align: middle;">No.</th>
                                            <th style="text-align: center;vertical-align: middle;">Nama Jobdesk</th>
                                            <th style="text-align: center;vertical-align: middle;">Jangka Waktu</th>
                                            <th style="text-align: center;vertical-align: middle;">Bobot Jobdesk</th>
                                            <th style="text-align: center;vertical-align: middle;">Status Jobdesk</th>
                                            <th style="text-align: center;vertical-align: middle;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1 ?>
                                        @foreach($data_pegawai as $pegawai)
                                        <tr>
                                            <td style="vertical-align: middle;text-align: center;">{{$no++}}</td>
                                            <td style="vertical-align: middle;text-align: left;">{{$pegawai->nama_jobdesk}}</td>
                                            <td style="vertical-align: middle;text-align: left;">{{$pegawai->waktu_jobdesk}}</td>
                                            <td style="vertical-align: middle;text-align: left;">{{$pegawai->bobot_jobdesk}}</td>
                                            <td style="vertical-align: middle;text-align: center;">{{$pegawai->status_jobdesk}}</td>
                                            <td style="text-align:center;">
                                                <button type="button" class="btn mb-1 btn-info btn-md" data-toggle="modal" data-target="#detailModal{{$pegawai->id_jobdesk}}">Detail</i>
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
<!-- Modal Tambah -->
<div class="modal fade" id="tambahModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="largeModalLabel">Tambah Jobdesk Pegawai</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="basic-form">
                            @foreach($namapegawai as $nama)
                            <form action="/pegawai/{{$nama->id_pegawai}}/pegawai/createjobdesk" method="POST">
                            @endforeach
                            {{csrf_field()}}
                            <div class="form-row">
                               <div class="col-5">
                                    <label>Nama Jobdesk :</label>
                                    <input type="text" class="form-control" name="nama_jobdesk" required maxlength="80">
                               </div>
                               <div class="col-4">
                                    <label>Jangka Waktu :</label>
                                    <input type="date" class="form-control" name="waktu_jobdesk" required maxlength="50">
                               </div>
                               <div class="col-3">
                                    <label>Bobot Jobdesk :</label>
                                    <input type="number" class="form-control" name="bobot_jobdesk" required>
                                </div>
                            </div>
                    </div>
                    </br>
                    <div class="basic-form">
                        <div class="form-row">
                            <div class="col-12">
                                <label>Deskripsi :</label>
                                <textarea class="form-control" name="deskripsi_jobdesk" required maxlength="200"></textarea>
                            </div>
                        </div>
                    </div>
                    @foreach($namapegawai as $nama)
                    <input type="hidden" name="pegawai_id" value="{{$nama->id_pegawai}}">
                    <input type="hidden" name="nama_pegawai" value="{{$nama->nama_pegawai}}">
                    @endforeach
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success">Tambah</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
<!-- End Modal tambah -->

<!-- Modal Detail -->
@foreach($data_pegawai as $pegawai)
<div class="modal fade" id="detailModal{{$pegawai->id_jobdesk}}" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="largeModalLabel">Detail Jobdesk</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="basic-form">
                            <form action="/pegawai/{{$pegawai->id_jobdesk}}/pegawai/createjobdesk" method="POST">
                            {{csrf_field()}}
                            <div class="form-row">
                               <div class="col-5">
                                    <label>Nama Jobdesk :</label>
                                    <input type="text" class="form-control" name="nama_jobdesk" readonly value="{{$pegawai->nama_jobdesk}}" maxlength="80">
                               </div>
                               <div class="col-4">
                                    <label>Jangka Waktu :</label>
                                    <input type="date" class="form-control" name="waktu_jobdesk" readonly value="{{$pegawai->waktu_jobdesk}}" maxlength="50">
                               </div>
                               <div class="col-3">
                                    <label>Bobot Jobdesk :</label>
                                    <input type="number" class="form-control" name="bobot_jobdesk" readonly value="{{$pegawai->bobot_jobdesk}}">
                                </div>
                            </div>
                    </div>
                    </br>
                    <div class="basic-form">
                        <div class="form-row">
                            <div class="col-12">
                                <label>Deskripsi :</label>
                                <textarea class="form-control" name="deskripsi_jobdesk" readonly maxlength="200">{{$pegawai->deskripsi_jobdesk}}</textarea>
                            </div>
                        </div>
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
@endforeach
<!-- End Modal Detail -->

<!-- Modal Edit -->
@foreach($data_pegawai as $pegawai)
<div class="modal fade" id="editModal{{$pegawai->id_jobdesk}}" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="largeModalLabel">Edit Data Pegawai</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="basic-form">
                            <form method="POST" action="/pegawai/{{$pegawai->id_jobdesk}}/editjobdesk">
                            {{csrf_field()}}
                            <div class="form-row">
                               <div class="col-5">
                                    <label>Nama Jobdesk :</label>
                                    <input type="text" class="form-control" name="nama_jobdesk" required value="{{$pegawai->nama_jobdesk}}" maxlength="80">
                               </div>
                               <div class="col-4">
                                    <label>Jangka Waktu :</label>
                                    <input type="date" class="form-control" name="waktu_jobdesk" required value="{{$pegawai->waktu_jobdesk}}" maxlength="50">
                               </div>
                               <div class="col-3">
                                    <label>Bobot Jobdesk :</label>
                                    <input type="number" class="form-control" name="bobot_jobdesk" required value="{{$pegawai->bobot_jobdesk}}">
                                </div>
                            </div>
                    </div>
                    </br>
                    <div class="basic-form">
                        <div class="form-row">
                            <div class="col-12">
                                <label>Deskripsi :</label>
                                <textarea class="form-control" name="deskripsi_jobdesk" required maxlength="200">{{$pegawai->deskripsi_jobdesk}}</textarea>
                            </div>
                        </div>
                    </div>
                    @foreach($namapegawai as $nama)
                    <input type="hidden" name="pegawai_id" value="{{$nama->id_pegawai}}">
                    <input type="hidden" name="nama_pegawai" value="{{$nama->nama_pegawai}}">
                    @endforeach
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
@endforeach
<!-- End Modal Edit -->

<!-- Modal Hapus -->
@foreach($data_pegawai as $pegawai)
<div class="modal fade" id="deleteModal{{$pegawai->id_jobdesk}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="exampleModalLabel">Status Jobdesk</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="/pegawai/{{$pegawai->id_jobdesk}}/statusjobdesk">
                {{csrf_field()}}
                <h7>
                    Apakah Jobdesk Ini Telah Selesai ?
                </h7><br>
                <?php $bobot = $pegawai->bobot_jobdesk; ?>
                @foreach($namapegawai as $nama)
                <?php $kinerja = $nama->kinerja_pegawai; ?>
                @endforeach
                <?php $total = $kinerja + $bobot; ?>
                <input type="hidden" name="kinerja_pegawai" value="<?= $total ?>">
                <input type="hidden" name="status_jobdesk" value="Selesai">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Selesai</button>
            </div>

        </form>

        </div>
    </div>                                     
</div>
@endforeach
<!-- End Modal Hapus -->
    @endsection