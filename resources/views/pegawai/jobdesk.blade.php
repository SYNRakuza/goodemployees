@extends('layouts.master')  
        @section('content')
<div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">{{$judul}}</strong>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;vertical-align: middle;">No.</th>
                                            <th style="text-align: center;vertical-align: middle;">NIP</th>
                                            <th style="text-align: center;vertical-align: middle;">Nama</th>
                                            <th style="text-align: center;vertical-align: middle;">Jabatan</th>
                                            <th style="text-align: center;vertical-align: middle;">Kinerja</th>
                                            <th style="text-align: center;vertical-align: middle;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1 ?>
                                        @foreach($data_pegawai as $pegawai)
                                        <tr>
                                            <td style="vertical-align: middle;text-align: center;">{{$no++}}</td>
                                            <td style="vertical-align: middle;text-align: left;">{{$pegawai->nip_pegawai}}</td>
                                            <td style="vertical-align: middle;text-align: left;">{{$pegawai->nama_pegawai}}</td>
                                            <td style="vertical-align: middle;text-align: left;">{{$pegawai->jabatan_pegawai}} {{$pegawai->bagian_pegawai}}</td>
                                            <td style="vertical-align: middle;text-align: center;">{{$pegawai->kinerja_pegawai}}</td>
                                            <td style="text-align:center;">
                                                <button type="button" class="btn mb-1 btn-info btn-md" onclick="window.location.href='/pegawai/{{$pegawai->user_id}}/jobdesk'">Jobdesk
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
    @endsection