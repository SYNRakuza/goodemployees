@include('layouts.includes._header')

@include('layouts.includes._sidebar')

@include('layouts.includes._navbar')

@yield('content')

@include('layouts.includes._script')