<!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./"><img src="/assets/images/logo.png" alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src="/assets/images/logo2.png" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <!-- <li class="active">
                        <a href="index.html"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                    </li> -->
                    <h3 class="menu-title">Pegawai</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown {{ (request()->is('dbpegawai*')) || (request()->is('dbmanager*')) || (request()->is('dbkepala*')) || (request()->is('dbkeuangan*')) || (request()->is('dbpenjualan*')) || (request()->is('dbpemasaran*')) || (request()->is('dbinformasi*')) ? 'active' : '' }}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-database"></i>Database</a>
                        <ul class="sub-menu children dropdown-menu">
                            @if(auth()->user()->role == 'Admin' || auth()->user()->role == 'Manager' )
                            <li><i class="fa fa-suitcase"></i><a href="/dbpegawai">Semua Pegawai</a></li>
                            <li><i class="fa fa-suitcase"></i><a href="/dbmanager">Manager</a></li>
                            @endif
                            @if(auth()->user()->role == 'Admin' || auth()->user()->role == 'Manager' || auth()->user()->role == 'Kepala Divisi')
                            <li><i class="fa fa-user"></i><a href="/dbkepala">Kepala Divisi</a></li>
                            @endif
                            <li><i class="fa fa-users"></i><a href="/dbkeuangan">Divisi Keuangan</a></li>
                            <li><i class="fa fa-users"></i><a href="/dbpenjualan">Divisi Penjualan</a></li>
                            <li><i class="fa fa-users"></i><a href="/dbpemasaran">Divisi Pemasaran</a></li>
                            <li><i class="fa fa-users"></i><a href="/dbinformasi">Divisi Informasi</a></li>
                        </ul>
                    </li>
                    @if(auth()->user()->role == 'Admin' || auth()->user()->role == 'Manager' || auth()->user()->role == 'Kepala Divisi')
                    <li class="menu-item-has-children dropdown {{ (request()->is('jdmanager*')) || (request()->is('jdkepala*')) || (request()->is('jdkeuangan*')) || (request()->is('jdpenjualan*')) || (request()->is('jdpemasaran*')) || (request()->is('jdinformasi*')) ? 'active' : '' }}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Jobdesk</a>
                        <ul class="sub-menu children dropdown-menu">
                            @if(auth()->user()->role == 'Admin')
                            <li><i class="fa fa-suitcase"></i><a href="/jdmanager">Manager</a></li>
                            @endif
                            @if(auth()->user()->role == 'Admin' || auth()->user()->role == 'Manager')
                            <li><i class="fa fa-user"></i><a href="/jdkepala">Kepala Divisi</a></li>
                            @endif
                            <li><i class="fa fa-users"></i><a href="/jdkeuangan">Divisi Keuangan</a></li>
                            <li><i class="fa fa-users"></i><a href="/jdpenjualan">Divisi Penjualan</a></li>
                            <li><i class="fa fa-users"></i><a href="/jdpemasaran">Divisi Pemasaran</a></li>
                            <li><i class="fa fa-users"></i><a href="/jdinformasi">Divisi Informasi</a></li>
                        </ul>
                    </li>
                    @endif
                    <li>
                        <a href="#"> <i class="menu-icon fa fa-laptop"></i>Project</a>
                    </li>
                    <!-- <li>
                        <a href="#"> <i class="menu-icon fa fa-th"></i>Buat Kuisioner</a>
                    </li> -->
                    @if(auth()->user()->role == 'Manager' || auth()->user()->role == 'Kepala Divisi' || auth()->user()->role == 'Divisi')
                    <h3 class="menu-title">My Profile</h3><!-- /.menu-title -->

                    <li class="{{(request()->is('myjobdesk*')) ? 'active' : '' }}">
                        <a href="/myjobdesk/{{auth()->user()->id}}" > <i class="menu-icon fa fa-tasks"></i>My Jobdesk</a>
                    </li>
                    <!-- <li>
                        <a href="widgets.html"> <i class="menu-icon fa fa-calendar"></i>My Attendance</a>
                    </li> -->
                    <li>
                        <a href="#"> <i class="menu-icon fa fa-laptop"></i>My Project</a>
                    </li>

                    <li>
                        <a href="#"> <i class="menu-icon fa fa-bar-chart"></i>My Statistics</a>
                    </li>
                    @endif
                    @if(auth()->user()->role == 'Admin')
                    <h3 class="menu-title">Admin</h3><!-- /.menu-title -->

                    <li class="{{(request()->is('akun*')) ? 'active' : '' }}">
                        <a href="/akun"> <i class="menu-icon fa fa-user"></i>Tambah Akun</a>

                    </li>
                    @endif
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->