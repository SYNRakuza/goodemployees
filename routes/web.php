<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PegawaiController@index')->middleware('auth');

//Sistem Login
Route::get('/login', 'AuthController@login')->name('login');
Route::post('/postlogin', 'AuthController@postlogin');
Route::get('/logout', 'AuthController@logout');

//Database Pegawai
Route::group(['middleware' => ['auth', 'checkRole:Admin']],function(){
	Route::get('/dbpegawai', 'PegawaiController@dbpegawai');
	Route::post('/pegawai/create', 'PegawaiController@create');
	Route::post('pegawai/{id_pegawai}/edit', 'PegawaiController@edit');
	Route::post('pegawai/{id_pegawai}/delete', 'PegawaiController@delete');
});

Route::group(['middleware' => ['auth', 'checkRole:Admin,Manager']],function(){
	Route::get('/dbpegawai', 'PegawaiController@dbpegawai');
	Route::get('/dbmanager', 'PegawaiController@dbmanager');
});

Route::group(['middleware' => ['auth', 'checkRole:Admin,Manager,Kepala Divisi']],function(){
	Route::get('/dbkepala', 'PegawaiController@dbkepala');
});

Route::group(['middleware' => ['auth', 'checkRole:Admin,Manager,Kepala Divisi,Divisi']],function(){
	Route::get('/dbkeuangan', 'PegawaiController@dbkeuangan');
	Route::get('/dbpenjualan', 'PegawaiController@dbpenjualan');
	Route::get('/dbpemasaran', 'PegawaiController@dbpemasaran');
	Route::get('/dbinformasi', 'PegawaiController@dbinformasi');
	Route::post('pegawai/{id_pegawai}/ban', 'PegawaiController@ban');
});

//Jobdesk Pegawai
Route::group(['middleware' => ['auth', 'checkRole:Admin']],function(){
	Route::get('/jdmanager', 'PegawaiController@jdmanager');
});
Route::group(['middleware' => ['auth', 'checkRole:Admin,Manager']],function(){
	Route::get('/jdkepala', 'PegawaiController@jdkepala');
});

Route::group(['middleware' => ['auth', 'checkRole:Admin,Manager,Kepala Divisi']],function(){
Route::get('/jdkeuangan', 'PegawaiController@jdkeuangan');
Route::get('/jdpenjualan', 'PegawaiController@jdpenjualan');
Route::get('/jdpemasaran', 'PegawaiController@jdpemasaran');
Route::get('/jdinformasi', 'PegawaiController@jdinformasi');
Route::get('pegawai/{id_pegawai}/jobdesk', 'PegawaiController@jobdesk');
Route::post('/pegawai/{id_pegawai}/pegawai/createjobdesk', 'PegawaiController@createjobdesk');
Route::post('/pegawai/{id_jobdesk}/editjobdesk', 'PegawaiController@editjobdesk');
Route::post('/pegawai/{id_jobdesk}/statusjobdesk', 'PegawaiController@statusjobdesk');
});

//My Jobdesk
Route::get('/myjobdesk/{id}', 'PegawaiController@myjobdesk')->middleware('auth');

//Profile
Route::group(['middleware' => ['auth']],function(){
	Route::get('/profil', 'PegawaiController@index');
	Route::post('/profil/{id_pegawai}/editprofil', 'PegawaiController@editprofil');
});

//Akun
Route::group(['middleware' => ['auth', 'checkRole:Admin']],function(){
	Route::get('/akun', 'PegawaiController@akun');
	Route::post('/admin/createadmin', 'PegawaiController@createadmin');
	Route::post('/admin/{$id}/editadmin', 'PegawaiController@editadmin');
});

